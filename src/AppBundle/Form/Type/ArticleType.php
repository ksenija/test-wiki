<?php

namespace AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ArticleType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        
        $builder->add('title', null, array(
            'attr' => array('class' => 'form-control'), 
            'label' => 'Название')
        );
        $builder->add('text', null, array(
            'attr' => array('class' => 'form-control'), 
            'label' => 'Текст')
        );
        
        $readonly = false;
        if(!$options["data"]->isNew()){
            $readonly = true;
        }
        
        $builder->add('path', null, array(
            'attr' => array('class' => 'form-control', 'readonly' => $readonly), 
            'label' => 'Алиас')
        );
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Model\Article',
        ));
    }

    public function getName()
    {
        return 'article';
    }
}