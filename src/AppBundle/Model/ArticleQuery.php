<?php

namespace AppBundle\Model;

use AppBundle\Model\Base\ArticleQuery as BaseArticleQuery;

/**
 * Skeleton subclass for performing query and update operations on the 'article' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class ArticleQuery extends BaseArticleQuery
{
    public static function getCurrentArticle($currentPath){
        
        if (!empty($currentPath)){
            $article = self::create()->findOneBy('path', $currentPath);
        }else{
            $article = self::create()->findRoot(); 
        }
        
        return $article;
    }
    
    
    
    
}
