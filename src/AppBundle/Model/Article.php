<?php

namespace AppBundle\Model;

use AppBundle\Model\Base\ArticleQuery as BaseArticleQuery;
use AppBundle\Model\Base\Article as BaseArticle;


/**
 * Skeleton subclass for representing a row from the 'article' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class Article extends BaseArticle
{
    public function add($parentArticle){
        $this->insertAsFirstChildOf($parentArticle);
        $this->save();
        $this->createPath();
    }
    
    public function wikiParser(){
        $text = preg_replace("!__(.*?)__!si","<u>\\1</u>",$this->getText());
        $text = preg_replace("!\*\*(.*?)\*\*!si","<b>\\1</b>",$text);
        $text = preg_replace("!\/\/(.*?)\/\/!si","<i>\\1</i>",$text);
        $text = preg_replace("!\"(.*?)\"!si","&#171;\\1&#187;",$text);
        
        $this->setText($text);
    }
    
    public function getPrepareDataItem(){
        $this->wikiParser();
        
        $is_root = $this->isRoot();
        $children = $this->getChildren()->toArray();
        $parent = $this->getParent();
        
        $itemArray = $this->toArray();
        $itemArray['parent'] = (empty($parent)) ? null : $parent->toArray();
        $itemArray['children'] = $children;
        $itemArray['is_root'] = $is_root;
        
        return $itemArray;
    }
    
    public function createPath(){
        $path = $this->getPath();
        if(empty($path)){
            $path = \AppBundle\AppBundle::translate($this->getTitle());
        }
        $parentArticlePath = $this->getParent()->getPath();
        if(!empty($parentArticlePath)){
            $path = $parentArticlePath . '/' . $path;
        }
        $searchNodeByPath = BaseArticleQuery::create()->findOneBy('path', $path);
        if(!empty($searchNodeByPath)){
            $path = $path . '-' . $this->getId();
        }
        $this->setPath($path);
        $this->save();
    }
}
