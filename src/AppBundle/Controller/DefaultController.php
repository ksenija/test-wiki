<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use AppBundle\Model\Article;
use AppBundle\Form\Type\ArticleType;
use AppBundle\Model\ArticleQuery;
use AppBundle;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        $currentArticle = ArticleQuery::getCurrentArticle($request->get('url'));
        $this->isExistAricle($currentArticle);
        
        return $this->render('default/index.html.twig', array(
            'current_item' => $currentArticle->getPrepareDataItem(),
            'base_url' => $request->getBaseUrl(),
        ));
    }
    
    
    public function addAction(Request $request)
    {
        $currentArticle = ArticleQuery::getCurrentArticle($request->get('url'));
        $this->isExistAricle($currentArticle);
        
        $art = new Article();
        $form = $this->createForm(new ArticleType(), $art);
        
        if ('POST' === $request->getMethod()) {
            $form->handleRequest($request);
            if ($form->isValid()) {
                $art->add($currentArticle);
                
                return $this->redirect($request->getBaseUrl() . "/" . $currentArticle->getPath());
            }
        }

        return $this->render('default/add.html.twig', array(
            'title' => 'Create article',
            'form' => $form->createView(),
        ));
    }
    
    public function editAction(Request $request)
    {
        $currentArticle = ArticleQuery::getCurrentArticle($request->get('url'));
        $this->isExistAricle($currentArticle);
        
        $form = $this->createForm(new ArticleType(), $currentArticle);
        
        if ('POST' === $request->getMethod()) {
            $form->handleRequest($request);

            if ($form->isValid()) {
                $currentArticle->save();
                
                return $this->redirect($request->getBaseUrl() . "/" . $currentArticle->getPath());
            }
        }
        
        return $this->render('default/add.html.twig', array(
            'title' => 'Edit '. $currentArticle->getTitle(),
            'form' => $form->createView(),
        ));
    }
    
    public function deleteAction(Request $request)
    {
        $currentArticle = ArticleQuery::getCurrentArticle($request->get('url'));
        $this->isExistAricle($currentArticle);
        
        $confirm = $request->get('confirm');
        if(!empty($confirm)){
            $parent = $currentArticle->getParent();
            $currentArticle->delete();
            
            return $this->redirect($request->getBaseUrl() . "/" . $parent->getPath());
        }
        
        return $this->render('default/delete.html.twig', array(
            'item' => $currentArticle->toArray(),
            'base_url' => $request->getBaseUrl(),
        ));
        
    }
    
    public function isExistAricle($currentArticle)
    {
        if (empty($currentArticle))
        {
            throw $this->createNotFoundException('The article does not exist');
        }
    }
    
    public function subtaskAction(Request $request){
        
        $form = $this->createFormBuilder()
            ->add('text', 'text', array('attr' => array('class' => 'form-control'), 'label' => 'Текст'))
            ->getForm();
        
        $result = '';
        if ('POST' === $request->getMethod()) {
            $data = $this->get('request')->request->get('form');
            if(isset($data['text'])){
                $result = preg_replace('/[^a-zа-яё0-9]+/iu', '', $data['text']);
            }
        }

        return $this->render('default/subtask.html.twig', array(
            'form' => $form->createView(),
            'result' => $result,
        ));
        
        
    }
    
    
}
